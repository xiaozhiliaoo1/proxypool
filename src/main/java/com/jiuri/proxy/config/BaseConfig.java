package com.jiuri.proxy.config;


/**
 * @program: proxy
 * @author: Mr.Han
 * @create: 2018-12-29 11:01
 **/
public class BaseConfig {
    private String host;
    private Integer spiderSwitch;
    private Integer startPageNum;
    private Integer endPageNum;
    private Integer timeout;
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getSpiderSwitch() {
        return spiderSwitch;
    }

    public void setSpiderSwitch(Integer spiderSwitch) {
        this.spiderSwitch = spiderSwitch;
    }

    public Integer getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(Integer startPageNum) {
        this.startPageNum = startPageNum;
    }

    public Integer getEndPageNum() {
        return endPageNum;
    }

    public void setEndPageNum(Integer endPageNum) {
        this.endPageNum = endPageNum;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }
}
