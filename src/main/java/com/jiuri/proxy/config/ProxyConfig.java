package com.jiuri.proxy.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @program: proxy
 * @author: Mr.Han
 * @create: 2018-12-29 15:18
 **/
@Configuration
@ConfigurationProperties("proxy.redis")
public class ProxyConfig {
    private String key;
    private Integer init;
    private Integer max;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getInit() {
        return init;
    }

    public void setInit(Integer init) {
        this.init = init;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }
}
