package com.jiuri.proxy.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("proxy.xici")
public class XiciConfig extends BaseConfig {
}