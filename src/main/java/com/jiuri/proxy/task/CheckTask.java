package com.jiuri.proxy.task;

import com.jiuri.proxy.async.CheckProxyAsync;
import com.jiuri.proxy.dao.ProxyDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.Date;
import java.util.Set;

/**
 * @program: proxy
 * @author: Mr.Han
 * @create: 2018-12-27 15:30
 **/
@Component
public class CheckTask {
    @Value("${proxy.checkSwitch}")
    private boolean checkSwitch;
    private static Logger LOGGER = LoggerFactory.getLogger(CheckProxyAsync.class);
    @Autowired
    private CheckProxyAsync checkProxyAsync;
    @Autowired
    private ProxyDAO proxyDAO;
    //验证ip
    @Scheduled(initialDelay = 2 * 1000,fixedRate = 60 * 1000)
    public void task(){
        if(!checkSwitch){
            return;
        }
        LOGGER.info("开始验证ip:"+new Date());
        Set<String> ipSet = proxyDAO.all();
        for (String o : ipSet) {
            String[] split = o.split(":");
            checkProxyAsync.check(split[0],Integer.parseInt(split[1]));
        }
    }
}
