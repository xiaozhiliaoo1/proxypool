package com.jiuri.proxy.api;

import com.jiuri.proxy.async.CheckProxySync;
import com.jiuri.proxy.dao.ProxyDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Set;

/**
 * @program: proxy
 * @author: Mr.Han
 * @create: 2018-12-28 15:37
 **/
@RestController
@RequestMapping("api")
public class ApiController {
    @Autowired
    private CheckProxySync spiderSync;
    @Autowired
    private ProxyDAO proxyDAO;
    /**
     * 随机取一个有效ip
     * @return
     */
    @RequestMapping("get")
    public String get(){
        for (int i = 0; i < 10; i++) {
            String proxy = proxyDAO.randomGetHighAvailableProxy();
            if(spiderSync.check(proxy.split(":")[0],Integer.parseInt(proxy.split(":")[1]))){
                return proxy;
            }
        }
        return null;
    }

    /**
     * 取所有ip（可能包含失效ip）
     * @return
     */
    @RequestMapping("getAll")
    public Set<String> getAllProxyList(){
        return proxyDAO.all();
    }
}
