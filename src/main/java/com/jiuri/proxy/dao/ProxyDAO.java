package com.jiuri.proxy.dao;

import cn.hutool.core.util.RandomUtil;
import com.jiuri.proxy.config.ProxyConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import java.util.Set;

@Service
public class ProxyDAO {
    
    private static Logger LOGGER = LoggerFactory.getLogger(ProxyDAO.class);
    @Autowired
    private ProxyConfig proxyConfig;

    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 判断当前代理是否已经存在
     *
     * @param proxy
     * @return
     */
    public boolean exists(String proxy) {
        return this.redisTemplate.opsForZSet().score(proxyConfig.getKey(), proxy) != null;
    }

    /**
     * 保存代理，并初始化代理的分值
     *
     * @param proxy
     * @return
     */
    public boolean save(String proxy) {
        if (!exists(proxy)) {
            LOGGER.info("===>>>保存代理:[{}]", proxy);
            return this.redisTemplate.opsForZSet().add(proxyConfig.getKey(), proxy, proxyConfig.getInit());
        }else if(score(proxy)<proxyConfig.getMax()){
            LOGGER.info("===>>>代理加分值:[{}]", proxy);
            redisTemplate.opsForZSet().incrementScore(proxyConfig.getKey(),proxy,+1);
            return true;
        }
        return false;
    }

    public void remove(String proxy){
        redisTemplate.opsForZSet().remove(proxyConfig.getKey(),proxy);
    }

    /**
     * 获取所有代理数
     *
     * @return
     */
    public long count() {
        return this.redisTemplate.opsForZSet().zCard(proxyConfig.getKey());
    }

    /**
     * 获取所有稳定的代理数
     *
     * @return
     */
    public long countHigh() {
        return this.redisTemplate.opsForZSet().rangeByScore(proxyConfig.getKey(), proxyConfig.getMax(), proxyConfig.getMax()).size();
    }

    /**
     * 获取全部代理
     *
     * @return
     */
    public Set<String> all() {
        return this.redisTemplate.opsForZSet().rangeByScore(proxyConfig.getKey(), proxyConfig.getInit(), proxyConfig.getMax());
    }

    /**
     * 获取redis中的所有键的集合
     *
     * @return
     */
    public Set<String> keys() {
        return this.redisTemplate.keys("*");
    }

    /**
     * 批量获取代理列表
     *
     * @return
     */
    public Set<Object> batchQuery(long start, long end) {
        return this.redisTemplate.opsForZSet().reverseRange(proxyConfig.getKey(), start, end);
    }

    /**
     * 将可用的代理设置为最大score
     *
     * @param proxy
     * @return
     */
    public boolean validOk(String proxy) {
        LOGGER.info("===>>>代理[{}]可用，设置为[{}]", proxy, proxyConfig.getMax());
        return this.redisTemplate.opsForZSet().add(proxyConfig.getKey(), proxy, proxyConfig.getMax());
    }

    /**
     * 从Redis中获取可用的代理
     * 优先获取最大分值的代理，如果没有找到最大分值代理，
     * 则最小最大区间获取可用代理，然后随机挑选一个代理返回给用户
     *
     * @return
     */
    public String random() {

        Set<Object> proxies = this.redisTemplate.opsForZSet().rangeByScore(proxyConfig.getKey(), proxyConfig.getMax(), proxyConfig.getMax());
        if (CollectionUtils.isEmpty(proxies)) {
            proxies = this.redisTemplate.opsForZSet().rangeByScore(proxyConfig.getKey(), proxyConfig.getInit(), proxyConfig.getMax());
        }

        if (CollectionUtils.isEmpty(proxies)) {
            return null;
        }

        return getRandomProxy(proxies).toString();
    }

    /**
     * 获取高可用的代理
     *
     * @return
     */
    public String randomGetHighAvailableProxy() {
        Set<Object> proxies = this.redisTemplate.opsForZSet().rangeByScore(proxyConfig.getKey(), proxyConfig.getMax(), proxyConfig.getMax());
        if (CollectionUtils.isEmpty(proxies)) {
            return null;
        }
        return getRandomProxy(proxies).toString();
    }

    /**
     * 获取当前代理的分值，如果代理不存在返回null
     *
     * @param proxy
     * @return
     */
    public Double score(String proxy) {
        return this.redisTemplate.opsForZSet().score(proxyConfig.getKey(), proxy);
    }



    /**
     * 随机从集合中获取一个元素
     *
     * @param set
     * @return
     */
    private Object getRandomProxy(Set<Object> set) {
        int rn = RandomUtil.randomInt(set.size());
        int i = 0;
        for (Object object : set) {
            if (i == rn) {
                return object;
            }
            i++;
        }
        return null;
    }


}
