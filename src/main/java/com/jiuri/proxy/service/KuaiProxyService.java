package com.jiuri.proxy.service;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.jiuri.proxy.async.CheckProxyAsync;
import com.jiuri.proxy.config.KuaiConfig;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @program: proxy
 * @author: Mr.Han
 * @create: 2018-12-25 17:51
 **/
@Service
public class KuaiProxyService{
    private static Logger LOGGER = LoggerFactory.getLogger(KuaiProxyService.class);
    @Autowired
    private KuaiConfig kuaiConfig;
    @Autowired
    private CheckProxyAsync checkProxyAsync;
    @Async
    public void kuaiProxy(String url) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        HttpResponse execute;
        try {
            execute = HttpUtil.createGet(url).timeout(kuaiConfig.getTimeout()).execute();
        }catch (Exception e){
            LOGGER.info("快代理请求失败,url:{}",url);
            return;
        }
        if(!execute.isOk()){
            LOGGER.info("访问"+url+"失败");
            return;
        }
        Document parse = Jsoup.parse(execute.body());
        Elements elements = parse.select("table tr");
        elements.remove(0);
        for (Element element : elements) {
            String ip = element.select("td:nth-child(1)").text();
            Integer port = Integer.parseInt(element.select("td:nth-child(2)").text());
            checkProxyAsync.check(ip,port);
        }
        //访问下一页链接
        String nextPage = nextPage(url);
        if(nextPage!=null){
            LOGGER.info("快代理 --- get page : "+nextPage);
            kuaiProxy(nextPage);
        }else{
            LOGGER.info("快代理 --- end");
        }
    }
    public String nextPage(String url){
        String[] split = null;
        if(url.contains("inha")){
            split = url.split("inha/");
        }
        if(url.contains("intr")){
            split = url.split("intr/");
        }
        String s = split[1].replaceAll("/", "");
        int i = Integer.parseInt(s);
        int b = i+1;
        if(b==kuaiConfig.getEndPageNum()){
            return null;
        }
        if(url.contains("inha")){
            return split[0]+"inha/"+b;
        }
        if(url.contains("intr")){
            return split[0]+"intr/"+b;
        }
        return null;
    }
}
